import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future signIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      print('user${result.user.email}');
    } catch (e) {
      print('error$e');
    }
  }

  listenEventChanges() {
    _auth.authStateChanges().listen((event) {
      print('userEvent=>$event');
      if (event == null) {
        print('userSignedOut');
      } else {
        print('user signed in');
      }
    });
  }

  Future signup(String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      print('error$e');
    }
  }

  signout() async {
    await _auth.signOut();
  }
}
