import 'package:diploma_project/utils/MyStyle.dart';
import 'package:flutter/material.dart';

class MyButton extends StatefulWidget {
  final String text;
  final Function onPressed;

  const MyButton({Key key, this.text, this.onPressed}) : super(key: key);
  @override
  _MyButtonState createState() => _MyButtonState();
}

class _MyButtonState extends State<MyButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        padding: EdgeInsets.all(12),
        textColor: Colors.white,
        onPressed: widget.onPressed,
        color: primaryColor,
        child: Text(
          widget.text ?? '',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        disabledColor: inactiveColor,
      ),
    );
  }
}
