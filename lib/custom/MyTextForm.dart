import 'package:diploma_project/utils/MyStyle.dart';
import 'package:flutter/material.dart';

class MyTextForm extends StatefulWidget {
  final String hint;
  final String labelText;
  final String initialText;
  final Function onChanged;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final bool isPassword;

  const MyTextForm({Key key, this.hint, this.labelText, this.initialText, this.onChanged, this.textInputType, this.textInputAction, this.isPassword}) : super(key: key);
  @override
  _MyTextFormState createState() => _MyTextFormState();
}

class _MyTextFormState extends State<MyTextForm> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: (text) {
        widget.onChanged(text);
      },
      obscureText: widget.isPassword ?? false,
      cursorColor: primaryColor,
      textInputAction: widget.textInputAction ?? TextInputAction.done,
      keyboardType: widget.textInputType ?? TextInputType.text,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: primaryColor),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        hintText: widget.hint,
        counterText: '',
        labelText: widget.labelText,
        hintStyle: TextStyle(
          fontSize: 16,
          letterSpacing: 0.5,
          color: hintColor,
        ),
        labelStyle: TextStyle(
          fontSize: 16,
          letterSpacing: 0.5,
          color: primaryColor,
        ),
      ),
    );
  }
}
