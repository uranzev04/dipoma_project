import 'package:diploma_project/utils/MyStyle.dart';
import 'package:flutter/material.dart';

class CircleButton extends StatefulWidget {
  final Function onPressed;
  final bool isGoogle;

  const CircleButton({Key key, this.onPressed, this.isGoogle = false}) : super(key: key);
  @override
  _CircleButtonState createState() => _CircleButtonState();
}

class _CircleButtonState extends State<CircleButton> {
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: widget.onPressed,
      elevation: 2,
      fillColor: widget.isGoogle == true ? colorGoogle : colorFacebook,
      padding: EdgeInsets.all(10),
      child: Text(
        widget.isGoogle == false ? 'f' : 'G',
        style: TextStyle(fontSize: 26, color: Colors.white),
      ),
      shape: CircleBorder(),
    );
  }
}
