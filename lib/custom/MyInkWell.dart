import 'package:flutter/material.dart';

///хурууны даралтанд ripple эффэкт өгөх хэсэг
class MyInkWell extends StatefulWidget {
  final Widget child;
  final BorderRadius borderRadius;
  final Color splashColor;
  final double opacity;
  final Function() onTap;
  final Color highlightColor;
  final bool borderRadiusOnly;
  final double topLeftRadius;
  final double bottomLeftRadius;
  final double topRightRadius;
  final double bottomRightRadius;
  MyInkWell({
    this.child,
    this.borderRadius = const BorderRadius.all(Radius.circular(4)),
    this.splashColor,
    this.opacity,
    this.onTap,
    this.highlightColor,
    this.borderRadiusOnly = false,
    this.topLeftRadius = 0,
    this.bottomLeftRadius = 0,
    this.topRightRadius = 0,
    this.bottomRightRadius = 0,
  });

  @override
  _MyInkWellState createState() => _MyInkWellState();
}

class _MyInkWellState extends State<MyInkWell> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        widget.child ?? Container(),
        Positioned.fill(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius: !widget.borderRadiusOnly
                  ? widget.borderRadius ?? BorderRadius.circular(7.5)
                  : BorderRadius.only(
                      topLeft: Radius.circular(widget.topLeftRadius),
                      topRight: Radius.circular(widget.topRightRadius),
                      bottomLeft: Radius.circular(widget.bottomLeftRadius),
                      bottomRight: Radius.circular(widget.bottomRightRadius),
                    ),
              splashColor: widget.splashColor?.withOpacity(0.4),
              highlightColor: widget.highlightColor,
              child: Container(),
              onTap: widget.onTap ?? () {},
            ),
          ),
        ),
      ],
    );
  }
}
