import 'package:diploma_project/backend/AuthService.dart';
import 'package:diploma_project/custom/CircleButton.dart';
import 'package:diploma_project/custom/MyButton.dart';
import 'package:diploma_project/custom/MyInkWell.dart';
import 'package:diploma_project/custom/MyTextForm.dart';
import 'package:diploma_project/utils/Constants.dart';
import 'package:diploma_project/utils/MyStyle.dart';
import 'package:diploma_project/utils/Strings.dart';
import 'package:diploma_project/utils/Utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  AuthService _authService = AuthService();
  String _email;
  String _password;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 50),
                Text(
                  strAppName,
                  style: TextStyle(
                    color: primaryColor,
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 24),
                MyTextForm(
                  labelText: strUserName,
                  onChanged: (text) {
                    _email = text;
                    setState(() {});
                  },
                ),
                SizedBox(height: 24),
                MyTextForm(
                  labelText: strPassword,
                  onChanged: (text) {
                    _password = text;
                    setState(() {});
                  },
                  isPassword: true,
                ),
                SizedBox(height: 24),
                MyButton(
                  text: strLogin,
                  onPressed: () => _signin(),
                ),
                SizedBox(height: 12),
                Text(
                  strUseSocial,
                  style: TextStyle(
                    color: secondaryTextColor,
                    fontSize: 14,
                  ),
                ),
                SizedBox(height: 24),
                Container(
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: Offset(0, 1),
                    ),
                  ], color: Colors.white, borderRadius: BorderRadius.circular(16)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleButton(
                        onPressed: () {},
                        isGoogle: true,
                      ),
                      CircleButton(
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 100),
                MyInkWell(
                  onTap: () => navigateRoute(context, signupRoute),
                  child: Text(
                    strSignup,
                    style: TextStyle(
                      fontSize: 22,
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _showHome() {
    navigateRoute(context, homeRoute);
  }

  _signin() {
    _authService.signIn(_email, _password);
  }
}
