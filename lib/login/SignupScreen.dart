import 'package:diploma_project/backend/AuthService.dart';
import 'package:diploma_project/custom/MyButton.dart';
import 'package:diploma_project/custom/MyTextForm.dart';
import 'package:diploma_project/utils/Strings.dart';
import 'package:flutter/material.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  AuthService _authService = AuthService();
  String _email;
  String _password;
  @override
  void initState() {
    super.initState();
    _authService.listenEventChanges();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(strSignup),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(24),
            child: Column(
              children: <Widget>[
                SizedBox(height: 24),
                MyTextForm(
                  labelText: strEmail,
                  onChanged: (text) {
                    _email = text;
                  },
                ),
                SizedBox(height: 24),
                MyTextForm(
                  labelText: strPassword,
                  isPassword: true,
                  onChanged: (text) {
                    _password = text;
                    setState(() {});
                  },
                ),
                SizedBox(height: 32),
                MyButton(
                  text: strSignup,
                  onPressed: () => _signup(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _signup() {
    _authService.signup(_email, _password);
  }
}
