import 'package:flutter/material.dart';

const Color primaryColor = const Color(0xfff88379);
const Color inactiveColor = Color(0xffC1C6D9);
const Color hintColor = Color(0x55000000);
const Color primaryTextColor = Color(0xff242726);
const Color secondaryTextColor = Color(0xff2e3233);
const Color colorFacebook = Color(0xff4267b2);
const Color colorGoogle = Color(0xffff6347);
