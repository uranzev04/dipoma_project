import 'package:flutter/material.dart';

Future<dynamic> navigateRoute(BuildContext context, String path, {bool remove = false, Object arguments, bool popUntil = false}) {
  if (popUntil) {
    Navigator.popUntil(context, ModalRoute.withName(path));
    return null;
  } else {
    if (remove)
      return Navigator.of(context).pushNamedAndRemoveUntil(path, (Route<dynamic> route) => false, arguments: arguments);
    else
      return Navigator.of(context).pushNamed(path, arguments: arguments);
  }
}
